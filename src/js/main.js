//= ../../bower_components/modernizr/modernizr.js
//= ../../bower_components/jquery/dist/jquery.js
//= ../../node_modules/owl.carousel/dist/owl.carousel.min.js

$('.videosection__videoslider').owlCarousel({
    loop:true,
    margin:18,
    nav:true,
    responsiveClass:true,
    responsive:{
        0:{
            items:2
        },
        700:{
            items:3
        }
    }
});
function scroll(id, m){
    var i = $('#'+id).offset();
     $('body,html').animate({
        scrollTop: i.top - m
    }, 300);
}
$('.videosection__videoslider_image').click(function(){
    if ($(this).hasClass('active')){}else{
        var id = $(this).parent().data('videoid');
        var name = $(this).siblings('.videosection__videoslider_name').text();
        $('.videosection__name').text(name);
        var src = "https://www.youtube.com/embed/"+id+"?controls=2&modestbranding=1&showinfo=0&autoplay=1";
        $('#videoframe').attr('src',src);
        $('.videosection__mainvideoblock .videodarkness').fadeOut();
        $('.videosection__videoslider_image').removeClass('active');
        $(this).addClass('active');
        scroll('videoframe', 50);
    }
});
$('.videosection__mainvideoblock .videodarkness').click(function(){
    var id = $(this).data('videoid');
    var src = "https://www.youtube.com/embed/"+id+"?controls=2&modestbranding=1&showinfo=0&autoplay=1";
    $('#videoframe').attr('src',src);
    $(this).fadeOut();
});
$('.terms__header_button').click(function(){
    $(this).parents('.terms').toggleClass('active');
});